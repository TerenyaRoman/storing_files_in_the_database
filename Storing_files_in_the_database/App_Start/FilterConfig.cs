﻿using System.Web;
using System.Web.Mvc;

namespace Storing_files_in_the_database
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
