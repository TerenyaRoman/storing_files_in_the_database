﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Models;

namespace Storing_files_in_the_database.Controllers
{
    public class HomeController : Controller
    {
        private MainContext _db;

        public HomeController()
        {
            _db = new MainContext();
        }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Загрузка файла
        /// </summary>
        /// <param name="uploadedFile"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase uploadedFile)
        {
            string error = "Не удалось сохранить файл. Попробуйте еще раз или обратитесь к системному администратору.";

            try
            {
                if (ModelState.IsValid)
                {
                    if (uploadedFile != null && uploadedFile.ContentLength > 0)
                    {
                        var model = new File
                        {
                            UploadTime = DateTime.Now,
                            Description = System.IO.Path.GetFileName(uploadedFile.FileName),
                            ContentType = uploadedFile.ContentType
                        };
                        using (var reader = new System.IO.BinaryReader(uploadedFile.InputStream))
                        {
                            model.Data = reader.ReadBytes(uploadedFile.ContentLength);
                        }
                        _db.File.Add(model);
                    }
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", error);
            }
            return View(error);
        }
        

        public ViewResult Files()
        {
            var files = _db.File.Where(e => e.Data != null).ToList();
            return View(files);
        }

        /// <summary>
        /// Скачивание файла
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FileContentResult Download(Guid id)
        {
            var fileToRetrieve = _db.File.Find(id);
            return File(fileToRetrieve.Data, fileToRetrieve.ContentType);
        }

        /// <summary>
        /// Удаление файла
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(Guid id)
        {
            _db.File.Remove(_db.File.SingleOrDefault(e => e.Id == id));
            _db.SaveChanges();

            return RedirectToAction("Files");
        }
    }
}