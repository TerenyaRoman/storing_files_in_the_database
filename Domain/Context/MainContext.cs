﻿using System.Data.Entity;
using Domain.Models;

namespace Domain.Context
{

    public class MainContext : DbContext
    {
        public MainContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            SetConfigurationOptions();
        }

        public MainContext() : this("Sfd")
        {
        }

        private void SetConfigurationOptions()
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<File> File { get; set; }
    }
}