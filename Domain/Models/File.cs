﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net.Mime;

namespace Domain.Models
{
    public class File
    {
        [Required]
        [Key, Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Дата загрузки
        /// </summary>
        public DateTime UploadTime { get; set; }

        /// <summary>
        /// Описание файла
        /// </summary>
        [MaxLength(128)]
        public string Description { get; set; }

        /// <summary>
        /// Тип файла
        /// </summary>
        [MaxLength(50)]
        public string ContentType { get; set; }

        /// <summary>
        /// Файл(данные)
        /// </summary>
        public byte[] Data { get; set; }
    }
}
